﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpssumEnemyy : MonoBehaviour
{
    [SerializeField] LayerMask blokLayer;
    public enum MOVI_DiRECTION
    {
        STOP,
        RIGHT,
        LEFT,
    }
    MOVI_DiRECTION movidirection = MOVI_DiRECTION.RIGHT;
    float speed　= 0;
    Rigidbody2D rb;
    private void FixedUpdate()
    {
        switch (movidirection)
        {
            case MOVI_DiRECTION.STOP:
                speed = 0;
                break;
            case MOVI_DiRECTION.RIGHT:
                speed = 3;
                transform.localScale = new Vector3(-1, 1, 1);
                break;
            case MOVI_DiRECTION.LEFT:
                speed = -3;
                transform.localScale = new Vector3(1, 1, 1);
                break;
        }
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (!IsGround())// ブール型関数なので！マーク　もしIsGroundだったらChangDirectionしなさい　２つの関数処理をアップデート関数で実行処理
        {
            ChangDirection();
        }
    }
    
    bool IsGround()//地面と当たってるか否かの判断をする線の関数処理
    {
        Vector3 startvec = transform.position - transform.right * 0.5f * transform.localScale.x;//ポジションを獲得しスタート位置を決めるローカルスケールで逆を向いた時線も逆を向くようにする
        Vector3 endVec = startvec - transform.up * 1f;//スタート位置を決めたら終点まで伸ばすと線ができる
        Debug.DrawLine(startvec, endVec);
        return Physics2D.Linecast(startvec, endVec, blokLayer);//ブール型なのでリターンが必要
    }
    void ChangDirection()//チャンジディレクション関数処理
    {
        if (movidirection == MOVI_DiRECTION.RIGHT)//もし右向いてたら左
        {
            movidirection = MOVI_DiRECTION.LEFT;//
        }
        else if (movidirection == MOVI_DiRECTION.LEFT)//もし左向いてたら右
        {
            movidirection = MOVI_DiRECTION.RIGHT;//
        }
            
    }
    public void DestroyEnemy()//敵死亡関数　※これをプレイヤーに使う
    {
        Destroy(this.gameObject);
    }


}