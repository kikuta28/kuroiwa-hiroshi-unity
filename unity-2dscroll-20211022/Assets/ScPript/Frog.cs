﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Frog : MonoBehaviour
{

    public float speed;
    bool m_xPlus = true;
    public float xpsition = 0;
    Animator animator;

    


    // Start is called before the first frame update
    void Start()
    {
         animator= GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (m_xPlus)
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0f, 0f);
            transform.localScale = new Vector3(-1, 1, 1);
            if (transform.position.x >= xpsition)
                m_xPlus = false;

        }
        else
        {
            transform.position -= new Vector3(speed * Time.deltaTime, 0f, 0f);
            transform.localScale = new Vector3(1, 1, 1);
            if (transform.position.x <= -xpsition)
                m_xPlus = true;
        }
    }
    public void DestroyFrog()//敵死亡関数　※これをプレイヤーに使う
    {
        Destroy(this.gameObject);
    }
}









/*public class Frog : MonoBehaviour
{
    public float speed;
    bool m_xPlus = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_xPlus)
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0f, 0f);
            if (transform.position.x >= 2)
                m_xPlus = false;
        }
        else
        {
            transform.position -= new Vector3(speed * Time.deltaTime, 0f, 0f);
            if (transform.position.x <= -2)
                m_xPlus = true;
        }
        
    }
}*/
