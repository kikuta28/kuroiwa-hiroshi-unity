﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plyer : MonoBehaviour
{

    [SerializeField] LayerMask blockLayer;

    public enum DIRECTION_TYPE
    {
        STOP,
        RIGHT,
        LEFT,
    }
    DIRECTION_TYPE direction = DIRECTION_TYPE.STOP;
    Rigidbody2D rb;
    float speed;
    public float jumpPower = 400;
    Animator animator;
    public float jumpPower1 = 200;
    public GameObject frog;


    public GameObject eagleEnemy;

    void Start()

    {

        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        eagleEnemy = GameObject.Find("eagleEnemyy");


    }


    void Update()
    {
        float x = Input.GetAxis("Horizontal");//方向キーの習得
        animator.SetFloat("speed", Mathf.Abs(x));
        if (x == 0)
        {
            direction = DIRECTION_TYPE.STOP;
        }
        if (x > 0)
        {
            direction = DIRECTION_TYPE.RIGHT;

        }
        if (x < 0)
        {
            direction = DIRECTION_TYPE.LEFT;
        }


        //Jump
        if (IsGround())
        {
            if (Input.GetKeyDown("space"))
            {
                Jump();
            }
            else
            {
                animator.SetBool("isJumping", false);
            }


        }
        if (Input.GetKeyDown("space"))
        {
            animator.SetBool("isJumping", true);
        }

    }

    private void FixedUpdate()//決まった感覚で呼ばれるもの
    {
        switch (direction)
        {
            case DIRECTION_TYPE.STOP:
                speed = 0;
                break;
            case DIRECTION_TYPE.RIGHT:
                speed = 3;
                transform.localScale = new Vector3(1, 1, 1);
                break;
            case DIRECTION_TYPE.LEFT:
                speed = -3;
                transform.localScale = new Vector3(-1, 1, 1);
                break;
        }
        rb.velocity = new Vector2(speed, rb.velocity.y);

    }

    void Jump()
    {
        rb.AddForce(Vector2.up * jumpPower);
        //animator.SetBool("isJumping", true);
    }
    bool IsGround()//地面についているか
    {
        //ベクトル視点と終点
        Vector3 leftStartPoint = transform.position - Vector3.right * 0.2f;
        Vector3 rightStartPoint = transform.position - Vector3.right * 0.2f;
        Vector3 endPoint = transform.position - Vector3.up * 0.1f;
        //Debug.DrawLine(始点、終点)；見えるか
        Debug.DrawLine(leftStartPoint, endPoint);
        Debug.DrawLine(rightStartPoint, endPoint);
        return Physics2D.Linecast(leftStartPoint, endPoint, blockLayer)//判定ありの矢印Physics2D.Linecast（始点、終点、あっているもの
            || Physics2D.Linecast(rightStartPoint, endPoint, blockLayer);




    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.name == "OpssumEnemyy")
        {
            GameObject opssum = collision.gameObject;//エネミー事態を習得
            if (transform.position.y > opssum.transform.position.y)//プレイヤーYがエネミーのYより大きかったら//ここではエネミーから関数を呼んでいたり位置関係があるためゲットコンポーネントしている
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);// いったん自身の縦方向の力を０にする　Xはそのまま
                                                            //の後にジャンプ
                rb.AddForce(Vector2.up * jumpPower1);
                opssum.GetComponent<OpssumEnemyy>().DestroyEnemy();
            }
            else
            {
                Destroy(gameObject);
            }

        }
        if (collision.gameObject.name == "eagleEnemyy")
        {
            if (transform.position.y > eagleEnemy.transform.position.y)
            {
                eagleEnemy.GetComponent<EagleEnemy>().DestroyEagle();
            }
            else
            {
                Destroy(gameObject);

            }




        }
    }
}







/*EagleEnemy eagleEnemy = collision.gameObject.GetComponent<EagleEnemy>();
if (transform.position.y > eagleEnemy.transform.position.y)
{
    rb.velocity = new Vector2(rb.velocity.x, 0);
    rb.AddForce(Vector2.up * jumpPower1);
    eagleEnemy.DestroyEagle();
}
else
{
    Destroy(gameObject);
}
Frog frog = collision.gameObject.GetComponent<Frog>();
if (transform.position.y > frog.transform.position.y)
{
    rb.velocity = new Vector2(rb.velocity.x, 0);
    rb.AddForce(Vector2.up * jumpPower1);
    frog.DestroyFrog();
}
else
{
    Destroy(gameObject);
}
*/





/*public float hill_tan()
{
    tan = 0f;
    if (Physics.Raycast(my.position, Plyer.up, out hit, shot_dis, layermask_1))
    {
        return_tan();
    }
    else
    {
        if (Physics.Raycast(my.position, -my.up, out hit, shot_dis * hosei, layermask_1))
        {
            return_tan();
        }
    }

    if (hit.normal.x == 1f || hit.normal.x == -1f)
    {
        tan = 0f;
    }

    return tan;
}

void return_tan()
{
    if (hit.normal.x > 0f)
    {
        tan = Mathf.PI * 0.5f + Mathf.Atan(hit.normal.y / Mathf.Abs(hit.normal.x));
    }
    else
    {
        tan = Mathf.PI * 0.5f - Mathf.Atan(hit.normal.y / Mathf.Abs(hit.normal.x));
    }
    tan = Mathf.Tan(tan);
}*/
