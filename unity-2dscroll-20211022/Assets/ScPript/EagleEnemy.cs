﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleEnemy : MonoBehaviour
{
    public float speed;
    bool m_xPlus = true;
    public float xpsition = 0;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (m_xPlus)
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0f, 0f);
            transform.localScale = new Vector3(-1, 1, 1);
            if (transform.position.x >= xpsition)
                m_xPlus = false;

        }
        else
        {
            transform.position -= new Vector3(speed * Time.deltaTime, 0f, 0f);
            transform.localScale = new Vector3(1, 1, 1);
            if (transform.position.x <= -xpsition)
                m_xPlus = true;
        }
    }
    public void DestroyEagle()//敵死亡関数　※これをプレイヤーに使う
    {
        Destroy(this.gameObject);
    }
}