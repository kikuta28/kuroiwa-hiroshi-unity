﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipperLeft : MonoBehaviour
{
    private HingeJoint hj;
    private JointSpring openSpring;//力を加える変数（箱）
    private JointSpring closeSpring;
    // Start is called before the first frame update
    void Start()
    {
        hj = GetComponent<HingeJoint>();
        openSpring = hj.spring;//力を加える箱の中にヒンジの力を代入した
        closeSpring = hj.spring;
        openSpring.targetPosition = hj.limits.max;//力を加えた者のポジションの動きはヒンジのMAXに依存します
        closeSpring.targetPosition = hj.limits.min;


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))//条件を入れ実行する
        {
            openFlipper();
            Debug.Log("aaa");
        }
        //GetKeyDownから、GetKeyUpに変更。
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            CloseSpring();
            Debug.Log("aaaq");
        }
    }
    public void openFlipper()//関数を作り実行する処理
    {
        hj.spring = openSpring;
    }
    public void CloseSpring()
    {
        hj.spring = closeSpring;
    }
}

/*public class Firipper : MonoBehaviour
{

    private HingeJoint hj;
    private JointSpring openSpring;//力をくわえる
    private JointSpring closeSpring;
    // Start is called before the first frame update
    void Start()
    {
        hj = GetComponent<HingeJoint>();
        openSpring = hj.spring;//動きをつけた
        closeSpring = hj.spring;

        openSpring.targetPosition = hj.limits.max;//ポジションを習得して、そこにヒンジマックスを代入
        closeSpring.targetPosition = hj.limits.min;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.O))
        {
            closeFlipper();
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Debug.Log("aaa");
            openFlipper();
        }
    }

    public void openFlipper()
    {
        hj.spring = openSpring;
    }
    public void closeFlipper()
    {
        hj.spring = closeSpring;
    }
}*/
